import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

const apiUrl = 'https://picsum.photos/v2/list';
const headerConfig = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
    providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  picture(): Observable<any> {
    return this.http.get<any>(apiUrl, headerConfig);
  }
}
